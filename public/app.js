import {
    get,
    set,
} from "https://cdn.jsdelivr.net/npm/idb-keyval@6/+esm";

let mediaMetadata = new MediaMetadata({});

async function pickSong(songName) {

    let audioPlayer = document.getElementById("audioPlayer")
    const coverImage = document.getElementById("audioImage")
    const audioTitle = document.getElementById("title")
    const audioArtist = document.getElementById("artist")
    const audioAlbum = document.getElementById("album")

    songName = songName.replaceAll(" ", "_");

    let songData = await fetch(`/record/${songName}`);
    songData = await songData.json();
    console.log(songData)

    audioPlayer.src = `../records/audio/${songName}.mp3`

    mediaMetadata = new MediaMetadata({
        title: songData.title,
        artist: songData.artist,
        album: songData.album,
        artwork: [
        {
            src: `../records/img/${songName}.jpg`,
            sizes: '256x256',
            type: 'image'
        },
        {
            src: `../records/img/${songName}.jpg`,
            sizes: '512x512',
            type: 'image'
        }
        ]
    });

    coverImage.src = mediaMetadata.artwork[0].src
    audioTitle.innerHTML = `<b>Title:</b> ${mediaMetadata.title}`
    audioArtist.innerHTML = `<b>Artist:</b> ${mediaMetadata.artist}`
    audioAlbum.innerHTML = `<b>Album:</b> ${mediaMetadata.album}`
}

window.pickSong = pickSong

if('mediaSession' in navigator) {
    const player = document.querySelector('audio');

    navigator.mediaSession.metadata = mediaMetadata
  
    navigator.mediaSession.setActionHandler('play', () => player.play());
    navigator.mediaSession.setActionHandler('pause', () => player.pause());
    navigator.mediaSession.setActionHandler('seekbackward', (details) => {
      const skipTime = details.seekOffset || 1;
      player.currentTime = Math.max(player.currentTime - skipTime, 0);
    });
  
    navigator.mediaSession.setActionHandler('seekforward', (details) => {
      const skipTime = details.seekOffset || 1;
      player.currentTime = Math.min(player.currentTime + skipTime, player.duration);
    });
  
    navigator.mediaSession.setActionHandler('seekto', (details) => {
      if (details.fastSeek && 'fastSeek' in player) {
        player.fastSeek(details.seekTime);
        return;
      }
      player.currentTime = details.seekTime;
    });
  
    navigator.mediaSession.setActionHandler('previoustrack', () => {
      player.currentTime = 0;
    });
}    

let pickImgButton = document.getElementById("pickImg")
let imgNameElement = document.getElementById("imgName")
pickImgButton.addEventListener('click', getImageFile)

const imgPickerOpts = {
    types: [
      {
        description: "Images",
        accept: {
          "image/jpeg": ['.jpg'],
        },
      },
    ],
    excludeAcceptAllOption: true,
    multiple: false,
  };
  
  let imageFileData = undefined;

  async function getImageFile() {
    
    try {
        // Open file picker and destructure the result the first handle
        const [fileHandle] = await window.showOpenFilePicker(imgPickerOpts);

        // get file contents
        imageFileData = await fileHandle.getFile();
        imgNameElement.innerText = imageFileData.name;
        imgNameElement.hidden = false;
        console.log(imageFileData.name)
    
    } catch (err) {
        console.log("User aborted or unexpected error occured.")
    }
  }


let pickSongButton = document.getElementById("pickSong")
let songNameElement = document.getElementById("songName")
pickSongButton.addEventListener('click', getSongFile)

const songPickerOpts = {
    types: [
      {
        description: "Audio",
        accept: {
          "audio/mpeg": ['.mp3'],
        },
      },
    ],
    excludeAcceptAllOption: true,
    multiple: false,
  };
  
  let songFileData = undefined;

  async function getSongFile() {
    
    try {
        // Open file picker and destructure the result the first handle
        const [fileHandle] = await window.showOpenFilePicker(songPickerOpts);

        // get file contents
        songFileData = await fileHandle.getFile();
        songNameElement.innerText = songFileData.name;
        songNameElement.hidden = false;
        songNameElement.style.color = 'black';

        console.log(songFileData.name)
    
    } catch (err) {
        console.log("User aborted or unexpected error occured.")
    }
  }
  

  const uploadButton = document.getElementById("btnUpload")
  const titleInput = document.getElementById("titleInput")
  const authorInput = document.getElementById("authorInput")
  const albumInput = document.getElementById("albumInput")

  uploadButton.addEventListener('click', uploadData)

  async function uploadData() {

    if(songFileData === undefined) {
        songNameElement.innerText = "Please pick a song first";
        songNameElement.hidden = false;
        songNameElement.style.color = 'red';
        return;
    }

    let title = titleInput.value;
    let author = authorInput.value;
    let album = albumInput.value;


    if(title === "") {
        title = songFileData.name.toLowerCase().replaceAll(" ", "_").replaceAll(".mp3", "")
    }

    let currentSongs = await fetch('/records')
    currentSongs = await currentSongs.json();

    currentSongs.files.forEach(fileName => {
        if(title.toLowerCase().replaceAll(" ", "_") === fileName.replaceAll(".mp3", "")) {
            alert("A track with this title already exists!")
            return;
        }
    });

    try {
        
        if(imageFileData === undefined) {
            imageFileData = await fetch("/assets/img/default_img.jpg")
            imageFileData = await imageFileData.blob()
        }

        let id = title.toLowerCase().replaceAll(" ", "_")
           await set(id, {
                title: title,
                author: author,
                album: album,
                image: imageFileData,
                song: songFileData
            })

            navigator.serviceWorker.ready.then((swRegistration) => {
                return swRegistration.sync.register(
                    "sync-songs"
                );
            });

            alert("Uploaded new song: " + title)
    } catch(err) {
        console.log(err)
    }

  }